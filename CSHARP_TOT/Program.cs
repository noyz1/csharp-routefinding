﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSHARP_TOT
{
    class MPosition
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public MPosition(double lat, double lng)
        {
            this.Lat = lat;
            this.Lng = lng;
        }
        public static double ComputeDistance(MPosition p1, MPosition p2)
        {
            double R = 6378.137;
            double dLat = p2.Lat * Math.PI / 180 - p1.Lat * Math.PI / 180;
            double dLon = p2.Lng * Math.PI / 180 - p1.Lng * Math.PI / 180;
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(p1.Lat * Math.PI / 180) * Math.Cos(p2.Lat * Math.PI / 180)
                * Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = R * c;
            return d * 1000; // meters
        }
    }
    enum ConnectionType
    {
        Walk,
        Bus
    }
    class Connection
    {
        public const double WALKING_WEIGHT = 20;
        public Stop destinationStop;
        public Stop sourceStop;
        public ConnectionType connectionType;
        public String routeCode;
        public Connection(Stop sourceStop, Stop destinationStop, ConnectionType connectionType)
        {
            this.destinationStop = destinationStop;
            this.connectionType = connectionType;
            this.sourceStop = sourceStop;
        }
        public Connection(Stop sourceStop, Stop destinationStop, ConnectionType connectionType, String routeCode)
        {
            this.destinationStop = destinationStop;
            this.connectionType = connectionType;
            this.sourceStop = sourceStop;
            this.routeCode = routeCode;
        }
        public override string ToString()
        {
            return "Source : " + sourceStop.stopName + "(" + sourceStop.code + ")" +
                "  " + "Destination : " + destinationStop.stopName + "(" + destinationStop.code + ")" + "  " +
               "Connection Type : " + connectionType + "  " + "route code : " + routeCode;
        }
        public static double computeCost(List<Connection> connections)
        {
            double outCost = 0;
            for (int i = 0; i < connections.Count; i++)
            {
                double thisCost = MPosition.ComputeDistance(connections[i].sourceStop.position, connections[i].destinationStop.position);
                if (connections[i].connectionType == ConnectionType.Walk)
                    thisCost *= Connection.WALKING_WEIGHT;
                outCost += thisCost;
            }
            return outCost;
        }
        public static List<Connection> FilterShowRoute(List<Connection> route)
        {
            if (route.Count <= 1)
                return route;
            List<Connection> outList = new List<Connection>();
            int start = 0;
            Connection lastConnection = route[0];
            Connection startConnection = route[0];
            for (int i = 1; i < route.Count; i++)
            {
                Connection thisConnection = route[i];
                if (thisConnection.connectionType != ConnectionType.Walk &&
                    thisConnection.routeCode.Equals(lastConnection.routeCode))
                {
                    start++;
                }
                else
                {
                    if(start > 0)
                    {
                        outList.Add(new Connection(startConnection.sourceStop, lastConnection.destinationStop,
                            startConnection.connectionType,(startConnection.routeCode + "**")));
                    }
                    else
                    {
                        outList.Add(lastConnection);
                    }
                    start = 0;
                    startConnection = thisConnection;
                }
                lastConnection = thisConnection;
            }
            if(start > 0)
            {
                outList.Add(new Connection(startConnection.sourceStop, lastConnection.destinationStop,
                           startConnection.connectionType, (startConnection.routeCode + "**")));
            }
            return outList;
        }
    }
    class Stop
    {
        public int code;
        public MPosition position;
        List<Connection> connections;
        public String stopName;
        public Stop(int code, double lat, double lng, String stopName)
        {
            connections = new List<Connection>();
            this.code = code;
            this.position = new MPosition(lat, lng);
            this.stopName = stopName;
        }
        public void addConnection(Stop dstStop,ConnectionType connectionType)
        {
            connections.Add(new Connection(this, dstStop, connectionType));
        }
        public void addConnection(Stop dstStop, ConnectionType connectionType, string routeCode)
        {
            connections.Add(new Connection(this, dstStop, connectionType, routeCode));
        }
        public double getDistanceToConnection(Connection connection)
        {
            Stop dstStop = connection.destinationStop;
            double distance = MPosition.ComputeDistance(this.position, dstStop.position);
            if (connection.connectionType == ConnectionType.Walk)
                distance *= Connection.WALKING_WEIGHT;
            return distance;
        }

        public static List<List<Connection>> Yens(Dictionary<int, Stop> stopDict, int src, int dst, int K)
        {
            HashSet<List<Connection>> B = new HashSet<List<Connection>>();
            List<List<Connection>> A = new List<List<Connection>>();
            A.Add(Dijkstra(stopDict, src, dst));
            for (int k = 1; k < K; k++)
            {
                for (int i = 0; i < A[0].Count; i++)
                {
                    int spurNode = A[k - 1][i].sourceStop.code;
                    List<Connection> rootPath = new List<Connection>();
                    for (int j = 0; j < i; j++)
                    {
                        rootPath.Add(A[k-1][j]);
                    }
                    Dictionary<int, bool> mask = new Dictionary<int,bool>();
                    List<Connection> removedConnections = new List<Connection>();
                    for (int kk = 0; kk < k; kk++)
                    {
                        bool same = true;
                        for (int ii = 0; ii < i; ii++)
                        {
                            if(A[kk][ii].sourceStop.code != rootPath[ii].sourceStop.code)
                            {
                                same = false;
                                break;
                            }
                        }
                        if (same)
                        {
                           /* removedConnections.Add(A[kk][i]);
                            A[kk][i].sourceStop.connections.Remove(A[kk][i]);*/
                            foreach(Connection con in A[kk][i].sourceStop.connections)
                            {
                                if(con.destinationStop.code == A[kk][i].destinationStop.code)
                                {
                                    removedConnections.Add(con);
                                }
                            }
                            foreach(Connection con in removedConnections)
                            {
                                A[kk][i].sourceStop.connections.Remove(con);

                            }
                        }
                    }
                    for (int ii = 0; ii < i; ii++)
                    {
                        mask.Add(rootPath[ii].sourceStop.code, true);
                    }
                    List<Connection> spurPath = Dijkstra(stopDict, spurNode, dst, mask);
                    List<Connection> totalPath = rootPath.Concat(spurPath).ToList();
                    B.Add(totalPath);
                    foreach(Connection con in removedConnections)
                    {
                        con.sourceStop.connections.Add(con);
                    }
                    removedConnections = new List<Connection>();
                }
                if (B.Count == 0)
                    break;
                double min_B_cost = -1;
                List<Connection> min_B_node = new List<Connection>();
                foreach(List<Connection> conSeq  in B)
                {
                    double thisCost = 0;
                    foreach(Connection con in conSeq)
                    {
                        thisCost += con.sourceStop.getDistanceToConnection(con);
                    }
                    if(min_B_cost == -1 || thisCost < min_B_cost)
                    {
                        min_B_cost = thisCost;
                        min_B_node = conSeq;
                    }
                }
                A.Add(min_B_node);
                B.Remove(min_B_node);
            }
            return A;
        }
        public static List<Connection> Dijkstra(Dictionary<int, Stop> stopDict,int src, int dst, Dictionary<int,bool> mask = null)
        {
            List<Stop> stops = stopDict.Values.ToList();
            Dictionary<int, int> prev = new Dictionary<int, int>();
            Dictionary<int, Connection> prevConnection = new Dictionary<int, Connection>();
            Dictionary<int, bool> visited = new Dictionary<int, bool>();
            Dictionary<int, double> dist = new Dictionary<int, double>();
            //bool[] visited = new bool[stopDict.Count];
            //visited[src] = true;
            dist[src] = 0;
            foreach (Stop stop in stops)
            {
                visited.Add(stop.code, false);
                if (stop.code != src)
                {
                    dist.Add(stop.code, -1);
                }
                prev.Add(stop.code, -1);
                prevConnection.Add(stop.code, null);
            }
            List <KeyValuePair<double, int>> queue = new List <KeyValuePair<double, int>>();
            queue.Add(new KeyValuePair<double, int>(dist[src], src));
            while(queue.Count>0 && !visited[dst])
            {
                queue = queue.OrderByDescending(t => t.Key).ToList();
                KeyValuePair<double, int> queueTop = queue[queue.Count-1];
                queue.RemoveAt(queue.Count - 1);
                Stop minStop = stopDict[queueTop.Value];
                if (visited[minStop.code])
                    continue;
                visited[minStop.code] = true;
                for (int i = 0; i < minStop.connections.Count; i++)
                {
                    if(mask != null)
                    {
                        if (mask.ContainsKey(minStop.connections[i].destinationStop.code) &&
                            mask[minStop.connections[i].destinationStop.code])
                            continue;
                    }
                    double alt = dist[minStop.code] + minStop.getDistanceToConnection(minStop.connections[i]);
                    int v = minStop.connections[i].destinationStop.code;
                        if (dist[v] == -1 || alt < dist[v])
                    {
                        dist[v] = alt;
                        prev[v] = minStop.code;
                        prevConnection[v] = minStop.connections[i];
                        queue.Add(new KeyValuePair<double, int>(alt, v));
                    }
                }


            }
            List<Connection> res = new List<Connection>();
            res.Add(prevConnection[dst]);
            Connection p = prevConnection[prevConnection[dst].sourceStop.code];
            while(p != null)
            {
                res.Add(p);
                p = prevConnection[p.sourceStop.code];
            }
            res.Reverse();
            return res;
        }
        
    }
    class Program
    {

        static void Main(string[] args)
        {
            DBConnect dBConnect = new DBConnect();
            dBConnect.OpenConnection();
            dBConnect.DropDatabase();
            dBConnect.CreateTable();
            Console.WriteLine("Database Init done");
            List<String> stopsRaw = readCSV("../../stops.csv");
            List<String> routesRaw = readCSV("../../stop_routes.csv");
            Dictionary<int, HashSet<KeyValuePair<int,string>>> cons = connectionExtractor(FilterConnectionRow(routesRaw));
            Dictionary<int,Stop> stopDict = stopDictExtractor(stopsRaw);
            //List<Stop> stops = stopDict.Values.ToList();
            //bus connections adder
            foreach (KeyValuePair<int, Stop> stopPair in stopDict)
            {
                Stop thisStop = stopPair.Value;
                if (!cons.ContainsKey(thisStop.code))
                    continue;
                HashSet<KeyValuePair<int, string>> thisStopCons = cons[thisStop.code];
                foreach(KeyValuePair<int, string> con in thisStopCons)
                {
                    Stop conStop = stopDict[con.Key];
                    thisStop.addConnection(conStop, ConnectionType.Bus,con.Value);
                }
            }
            int count = 0;

            foreach (KeyValuePair<int, Stop> stPaira in stopDict)
            {
                Stop a = stPaira.Value;
                foreach (KeyValuePair<int, Stop> stPairb in stopDict)
                {
                    Stop b = stPairb.Value;
                    if (a.Equals(b))
                        continue;
                    double dis = MPosition.ComputeDistance(a.position, b.position);
                    if (dis < 500)
                    {
                        count++;
                        a.addConnection(b, ConnectionType.Walk);
                    }
                }
            }
            Console.WriteLine("reading files done");
            while (true)
            {
                Console.Write("enter src : ");
                int src = int.Parse(Console.ReadLine());
                Console.Write("enter dst : ");
                int dst = int.Parse(Console.ReadLine());
                Console.Write("enter number of routes to print : ");
                int k = int.Parse(Console.ReadLine());
                List<List<Connection>> A = Stop.Yens(stopDict, src, dst, k);
                for (int kk = 0; kk < A.Count; kk++)
                {
                    var a = A[kk];
                    List<Connection> b = Connection.FilterShowRoute(a);
                    for (int i = 0; i < b.Count; i++)
                    {
                        Connection c = b[i];
                        dBConnect.addRow(src, dst, c.sourceStop.stopName, c.destinationStop.stopName,
                            c.connectionType.ToString(), i + 1, c.routeCode, kk + 1); // writing to database
                        Console.WriteLine(c.ToString());
                    }
                    Console.WriteLine("Cost is : "  + Connection.computeCost(a).ToString());
                    Console.WriteLine();
                    Console.WriteLine();
                }
                Console.WriteLine(A[0].ToString());
            }

        }
        public class SequenceNumberException : Exception
        {
            public SequenceNumberException()
            {
            }

            public SequenceNumberException(string message)
                : base(message)
            {
            }

            public SequenceNumberException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        static Dictionary<int, HashSet<KeyValuePair<int,string>>> connectionExtractor(List<String> connectionsRaw)
        {
            Dictionary<int, HashSet<KeyValuePair<int, string>>> connectionDict = new Dictionary<int, HashSet<KeyValuePair<int, string>>>();//stop code to stop code list
            String titlesRaw = connectionsRaw[0];
            string[] titles = titlesRaw.Split(',');
            int routeCodeCol = Array.FindIndex(titles, item => item == "route_code");
            int stopCodeCol = Array.FindIndex(titles, item => item == "stop_code");
            int sequenceNumberCol = Array.FindIndex(titles, item => item == "sequence_number");

            int lastStopCode = -1;
            int lastSequenceNumber = -1;
            string lastRouteCode = null;
            for (int i = 1; i < connectionsRaw.Count; i++)
            {
                string[] splittedRow = connectionsRaw[i].Split(',');
                int stopCode = int.Parse(splittedRow[stopCodeCol]);
                int sequenceNumber = int.Parse(splittedRow[sequenceNumberCol]);
                string routeCode = (splittedRow[routeCodeCol]);
                if (i > 1 && lastRouteCode.Equals(routeCode))
                {
                    if (lastSequenceNumber != sequenceNumber - 1)
                        throw new SequenceNumberException("sequence not followed");
                    if (!connectionDict.ContainsKey(lastStopCode))
                    {
                        connectionDict.Add(lastStopCode, new HashSet<KeyValuePair<int, string>>());
                    }

                    connectionDict[lastStopCode].Add(new KeyValuePair<int,string>(stopCode, routeCode));
                }
                lastStopCode = stopCode;
                lastSequenceNumber = sequenceNumber;
                lastRouteCode = routeCode;

            }
            return connectionDict;
        }
        
        static Dictionary<int,Stop> stopDictExtractor(List<String> stopsRaw)
        {
            Dictionary<int, Stop> stopsDict = new Dictionary<int, Stop>();
            String titlesRaw = stopsRaw[0];
            string[] titles = titlesRaw.Split(',');
            int stopCol = Array.FindIndex(titles, item => item == "stop_code");
            int positionCol = Array.FindIndex(titles, item => item == "position");
            int stopNameCol = Array.FindIndex(titles, item => item == "stop_name");
            for (int i = 1; i < stopsRaw.Count; i++)
            {
                string[] splittedRow = stopsRaw[i].Split(',');
                int stopCode = int.Parse(splittedRow[stopCol]);
                String positionString = splittedRow[positionCol];
                String stopName = splittedRow[stopNameCol];
                String latLong = positionString.Split('(')[1].Split(')')[0];
                double lng = double.Parse(latLong.Split(' ')[0]);
                double lat = double.Parse(latLong.Split(' ')[1]);
                stopsDict.Add(stopCode,new Stop(stopCode, lat, lng, stopName));
            }
            return stopsDict;
        }
        static List<Stop> stopExtractor(List<String> stopsRaw)
        {
            List<Stop> stops = new List<Stop>();
            String titlesRaw = stopsRaw[0];
            string[] titles = titlesRaw.Split(',');
            int stopCol = Array.FindIndex(titles, item => item == "stop_code");
            int positionCol = Array.FindIndex(titles, item => item == "position");
            int stopNameCol = Array.FindIndex(titles, item => item == "stop_name");
            for (int i = 1; i < stopsRaw.Count; i++)
            {
                string[] splittedRow = stopsRaw[i].Split(',');
                int stopCode = int.Parse(splittedRow[stopCol]);
                String positionString = splittedRow[positionCol];
                String stopName = splittedRow[stopNameCol];
                String latLong = positionString.Split('(')[1].Split(')')[0];
                double lng = double.Parse(latLong.Split(' ')[0]);
                double lat = double.Parse(latLong.Split(' ')[1]);
                stops.Add(new Stop(stopCode, lat, lng, stopName));
            }

            return stops;
        }
        static List<String> FilterConnectionRow(List<String> connectionList)
        {
            String titlesRaw = connectionList[0];
            string[] titles = titlesRaw.Split(',');
            int routeCodeCol = Array.FindIndex(titles, item => item == "route_code");
            int stopCodeCol = Array.FindIndex(titles, item => item == "stop_code");
            int sequenceNumberCol = Array.FindIndex(titles, item => item == "sequence_number");
            List<String> filteredList = new List<String>();
            filteredList.Add(titlesRaw);
            for (int i = 1; i < connectionList.Count; i++)
            {
                string[] splittedRow = connectionList[i].Split(',');
                int stopCode = int.Parse(splittedRow[stopCodeCol]);
                int sequenceNumber = int.Parse(splittedRow[sequenceNumberCol]);
                string routeCode = (splittedRow[routeCodeCol]);
                if (routeCode.Substring(0, 2).Equals("DR"))
                    continue;
                filteredList.Add(connectionList[i]);

            }
            return filteredList;
        }
                
        static List<String> readCSV(String csvPath)
        {
            using (var reader = new StreamReader(csvPath))
            {
                List<String> outList = new List<String>();
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    outList.Add(line);
                }
                return outList;

            }
        }
    }
}

class DBConnect
{
    private MySqlConnection connection;
    private string server;
    private string database;
    private string uid;
    private string password;
    private string port;

    //Constructor
    public DBConnect()
    {
        Initialize();
    }

    //Initialize values
    private void Initialize()
    {
        server = "localhost";
        port = "3306";
        database = "routes";
        uid = "root";
        password = "asghar12";
        string connectionString;
        connectionString = "SERVER=" + server + ";" + "DATABASE=" +
        database + ";" + "Port=" +
        port + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

        connection = new MySqlConnection(connectionString);
    }

    //open connection to database
    public bool OpenConnection()
    {
        try
        {
            connection.Open();
            return true;
        }
        catch (MySqlException ex)
        {
            //When handling errors, you can your application's response based 
            //on the error number.
            //The two most common error numbers when connecting are as follows:
            //0: Cannot connect to server.
            //1045: Invalid user name and/or password.
            switch (ex.Number)
            {
                case 0:
                    Console.WriteLine("Cannot connect to server.  Contact administrator");
                    break;

                case 1045:
                    Console.WriteLine("Invalid username/password, please try again");
                    break;
            }
            return false;
        }
    }
    public void CreateTable()
    {
        MySqlCommand createTableCommand = new MySqlCommand(@"
                               CREATE TABLE IF NOT EXISTS routes.Routes 
        (startNode int,
        endNode int,
        sourceStopName varchar(50),
        destinationStopName varchar(50),
        ConnectionType varchar(20),
        sequence int,
        routeCode varchar(50),
        K int,
        primary key(startNode, endNode, sequence,K))
        COLLATE='utf8_general_ci' ENGINE=InnoDB;", connection);
        createTableCommand.ExecuteNonQuery();
    }
    public void DropDatabase()
    {

        MySqlCommand createTableCommand = new MySqlCommand(@"DROP TABLE IF EXISTS `Routes`;", connection);
        createTableCommand.ExecuteNonQuery();
    }
    public void addRow(int startNode,int endNode, String sourceStopName, String destinationStopName,
        String ConnectionType, int sequence, String routeCode,int K)
    {
        MySqlCommand cmd = new MySqlCommand();
        cmd.Connection = connection;
        cmd.CommandText =
            "INSERT INTO Routes(startNode,endNode,sourceStopName,destinationStopName,ConnectionType,sequence,routeCode, K)";
        cmd.CommandText+= " VALUES(?startNode,?endNode,?sourceStopName,?destinationStopName,?ConnectionType,?sequence,?routeCode,?K)";
        cmd.Parameters.Add("?startNode", MySqlDbType.Int32).Value = startNode;
        cmd.Parameters.Add("?endNode", MySqlDbType.Int32).Value = endNode;
        cmd.Parameters.Add("?sourceStopName", MySqlDbType.VarChar).Value = sourceStopName;
        cmd.Parameters.Add("?destinationStopName", MySqlDbType.VarChar).Value = destinationStopName;
        cmd.Parameters.Add("?ConnectionType", MySqlDbType.VarChar).Value = ConnectionType;
        cmd.Parameters.Add("?sequence", MySqlDbType.Int32).Value = sequence;
        cmd.Parameters.Add("?routeCode", MySqlDbType.VarChar).Value = routeCode;
        cmd.Parameters.Add("?K", MySqlDbType.Int32).Value = K;
        cmd.ExecuteNonQuery();

    }
}